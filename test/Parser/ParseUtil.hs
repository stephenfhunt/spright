module Parser.ParseUtil (expectSuccess,
                         expectFailure) where

import Test.HUnit

import Spright.Parser (parseSprightText)
import qualified Spright.Ast as Ast
import SprightTestUtil

expectSuccess text expectation = case parseSprightText text of 
                                 Left e -> assertFailure $ "Parse failed with " ++ show e
                                 Right result -> expectation result @? show result 

expectFailure text = case parseSprightText text of 
                        Left _ -> return ()
                        Right _ -> assertFailure $ "Parse succeeded on " ++ text

