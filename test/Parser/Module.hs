module Parser.Module (moduleParseTest) where

import Test.Framework
import Test.Framework.Providers.HUnit

import Spright.Parser (parseSprightText)
import qualified Spright.Ast as Ast
import Parser.ParseUtil
import SprightTestUtil

-- Utility functions for poking around module structures 
moduleName (Ast.Module name _ _) = name
moduleName (Ast.ClassModule _ (Ast.Class name _ )) = name

expectModule text expected = expectSuccess text $ (==) expected

moduleParses name = do { text <- readFile $ goodFile name 
                       ; astText <- readFile $ goodAst name
                       ; expectModule text $ read astText
                       }

moduleFails filename = do { text <- readFile $ badFile filename
                          ; expectFailure text
                          }

expectModuleName text name = expectModule text $ Ast.Module name [] []
expectClassModuleName text name = expectModule text $ Ast.ClassModule [] $ Ast.Class name [] 

plainModuleHeaderIsValid = testGroup "A plain module header is valid"
                           [ testCase "just module header" $ expectModuleName "module ModuleName" "ModuleName"
                           , testCase "leading spaces" $ expectModuleName "    module ModuleName" "ModuleName"
                           , testCase "leading newlines" $ expectModuleName "\n\n\n\nmodule ModuleName" "ModuleName"
                           , testCase "header comment" $ expectModuleName "/* A comment at the beginning */module ModuleName" "ModuleName"
                           , testCase "leading line comment" $ expectModuleName "// A line comment at the beginning\nmodule ModuleName" "ModuleName"
                           ]

emptyFileIsRejected = testCase "An empty file is rejected" $ expectFailure "" 

topLevelClassIsValid = testGroup "A top-level class is a valid module"
                       [ testCase "simple empty class" $ expectClassModuleName "class Foo {}" "Foo"
                       , testCase "leading whitespace" $ expectClassModuleName "    \t\nclass Foo {}" "Foo"
                       , testCase "leading public" $ expectClassModuleName "public class Foo {}" "Foo"
                       , testCase "leading private visibility" $ expectFailure "private class Foo {}"
                       , testCase "leading module visibility" $ expectFailure "module class Foo {}"
                       , testCase "leading comment" $ expectClassModuleName "/* description */ class Foo {}" "Foo"
                       , testCase "missing class body" $ expectFailure "class Foo" 
                       , testCase "leading single import" $ expectModule
                                                            "import A\nclass Foo {}" $ 
                                                            Ast.ClassModule [Ast.Import ["A"]] $ Ast.Class "Foo" []
                       , testCase "leading imports" $ expectModule 
                                                      "import A\nimport B\n\n class Foo {}" $
                                                      Ast.ClassModule [Ast.Import ["A"], Ast.Import ["B"]] $ Ast.Class "Foo" []
                       ]

moduleCanHaveImports = testGroup "A module can import other modules"
                       [ testCase "single import" $ expectModule "module F\nimport G\n" $ Ast.Module "F" [Ast.Import ["G"]] []
                       , testCase "nothing before imports" $ expectFailure "module ModuleName\ngobbledygook\nimport G"
                       , testCase "multiple imports" $ expectModule "module F\nimport G\nimport H\nimport I\n" $ 
                                                                    Ast.Module "F" [Ast.Import ["G"], Ast.Import ["H"], Ast.Import ["I"]] [] 
                       , testCase "but nothing interleaved" $ expectFailure "module F\nimport G\nflibbertygibbet\nimport H"
                       ]

moduleCanHaveClasses = testGroup "A module can contain classes"
                       [ testCase "single class" $ moduleParses "SingleClass"
                       , testCase "multiple classes" $ moduleParses "MultiClass"
                       , testCase "a partial class fails" $ moduleFails "PartialClass"
                       ]

moduleCanHaveFunctions = testGroup "A module can contain functions"
                         [ testCase "single function" $ moduleParses "SingleFunction"
                         , testCase "multiple functions" $ moduleParses "MultipleFunctions"
                         , testCase "partial function" $ moduleFails "PartialFunction"
                         , testCase "mixed functions and classes" $ moduleParses "MixedClassFunctions"
                         ]

moduleParseTest = testGroup "Module parsing tests" 
                  [ plainModuleHeaderIsValid
                  , emptyFileIsRejected
                  , topLevelClassIsValid
                  , moduleCanHaveImports
                  , moduleCanHaveClasses
                  , moduleCanHaveFunctions
                  ]
