import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import qualified Parser.Module

main = defaultMain
       [ Parser.Module.moduleParseTest 
       ]
