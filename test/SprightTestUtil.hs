module SprightTestUtil where

import System.FilePath (joinPath, addExtension)

testPath sub extension name = joinPath ["test", "spright_files", 
                                        sub, addExtension name extension]

goodFile = testPath "good" "spright"
goodAst = testPath "good" "ast"
badFile = testPath "bad" "spright"

composePred a b object = a object && b object
