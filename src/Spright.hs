module Main where

import Spright.Parser.Module
import System.Environment
import Spright.Parser (parseSprightFile)

main = do 
       { args <- getArgs
       ; fileResult <- parseSprightFile $ head args 
       ; let result = case fileResult of 
                        Left e -> show e 
                        Right m -> show m
         in putStrLn result 
       }
