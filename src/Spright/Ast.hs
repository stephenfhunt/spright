module Spright.Ast where

data Program = Program Module deriving (Eq, Show, Read)

data Module = Module String [Import] [ModuleMember] |
              ClassModule [Import] Class deriving (Eq, Show, Read)

data Import = Import [String] deriving (Eq, Show, Read)

data ModuleMember = ModuleFunction Function | ModuleClass Class deriving (Eq, Show, Read)

data Function = Function Type ParamList FunctionBody String deriving (Eq, Show, Read)

data ParamList = ParamList [(String, Type)] deriving (Eq, Show, Read)

data FunctionBody = FunctionBody deriving (Eq, Show, Read) -- TODO

data Class = Class String [ClassMember] deriving (Eq, Show, Read)

data ClassMember = MemberFunction Function | DataMember Field deriving (Eq, Show, Read)

data Field = Field Type String deriving (Eq, Show, Read)

data Type = Type String deriving (Eq, Show, Read)
