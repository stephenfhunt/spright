module Spright.Parser.Function (functionDeclaration) where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Combinator
import qualified Text.ParserCombinators.Parsec.Token as Token
import Spright.Parser.Language (sprightLexer, moduleVisibility)
import qualified Spright.Ast as Ast

functionDeclaration = do { moduleVisibility
                         ; name <- Token.identifier sprightLexer
                         ; params <- Token.parens sprightLexer paramList
                         ; funcType <- functionType
                         ; body <- functionBody
                         ; return $ Ast.Function funcType params body name
                         }

functionType = do { Token.colon sprightLexer
                  ; typeName <- Token.identifier sprightLexer
                  ; return $ Ast.Type typeName
                  }

paramList = fmap Ast.ParamList $ Token.commaSep sprightLexer $ param

param = do { name <- Token.identifier sprightLexer
           ; Token.colon sprightLexer
           ; paramType <- Token.identifier sprightLexer
           ; return (name, Ast.Type paramType)
           }

functionBody = do { Token.symbol sprightLexer "{"
                  ; Token.symbol sprightLexer "}"
                  ; return Ast.FunctionBody
                  }
