module Spright.Parser.Class where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Combinator (optional)
import Text.ParserCombinators.Parsec.Token
import qualified Spright.Parser.Language as SprightLang
import Spright.Parser.Import (importList)
import qualified Spright.Ast as Ast

-- a top-level class not visible outside the module would be impossible to use
moduleLevelClass = do { imports <- importList
                      ; cls <- classWithVisibility (optional $ reserved SprightLang.sprightLexer "public")
                      ; return $ Ast.ClassModule imports cls
                      }

classDeclaration = classWithVisibility SprightLang.moduleVisibility

classWithVisibility v = do
                        { v
                        ; reserved SprightLang.sprightLexer "class"
                        ; name <- identifier SprightLang.sprightLexer
                        ; reservedOp SprightLang.sprightLexer "{"
                        -- TODO class body, use Token.braces
                        ; reservedOp SprightLang.sprightLexer "}"
                        ; return $ Ast.Class name []
                        }

