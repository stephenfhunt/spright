module Spright.Parser.Import where

import Text.ParserCombinators.Parsec.Char
import Text.ParserCombinators.Parsec.Combinator
import Text.ParserCombinators.Parsec.Prim
import Text.ParserCombinators.Parsec.Token
import Spright.Parser.Language (sprightLexer)
import qualified Spright.Ast as Ast

importList = do { imports <- endBy singleImport $ many1 $ oneOf "\r\n"
                ; whiteSpace sprightLexer
                ; return imports
                }

singleImport = do { reserved sprightLexer "import" 
                  ; modulePath <- sepBy (many1 (letter <|> char '_')) (char '.')
                  ; many $ oneOf " \t\f\v" -- non-newline whitespace
                  ; return $ Ast.Import modulePath
                  } 

