module Spright.Parser.Language (sprightLexer,
                               moduleVisibility,
                               classVisibility) where

import Data.Maybe (maybe)
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Language
import Text.ParserCombinators.Parsec.Combinator
import qualified Text.ParserCombinators.Parsec.Token as Token

sprightDef = emptyDef { Token.commentStart = "/*"
                     , Token.commentEnd = "*/"
                     , Token.commentLine = "//"
                     , Token.nestedComments = True
                     , Token.identStart = letter <|> char '_'
                     , Token.identLetter = alphaNum <|> char '_'
                     , Token.reservedNames = ["module", 
                                              "import",
                                              "class", 
                                              "test", 
                                              "public",
                                              "private",
                                              "protected",
                                              "if",
                                              "else",
                                              "while",
                                              "case"]
                     , Token.caseSensitive = True
                     }

sprightLexer = Token.makeTokenParser sprightDef

visibility options = do { result <- optionMaybe $ choice [Token.symbol sprightLexer o | o <- options] 
                        ; return $ maybe "module" id result
                        }

moduleVisibilityOptions = [ "public", "module", "test" ]
moduleVisibility = visibility moduleVisibilityOptions <?> "module-scoped visibility"
classVisibilityOptions = moduleVisibilityOptions ++ ["protected", "private"]
classVisibility = visibility classVisibilityOptions <?> "class-scoped visibility"

