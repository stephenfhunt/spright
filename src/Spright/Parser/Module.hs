module Spright.Parser.Module where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Token
import Spright.Parser.Language (sprightLexer)
import Spright.Parser.Class (moduleLevelClass, classDeclaration)
import Spright.Parser.Import (importList)
import Spright.Parser.Function (functionDeclaration)
import qualified Spright.Ast as Ast

sprightModule = do { whiteSpace sprightLexer
                  ; try moduleDeclaration <|>
                    moduleLevelClass 
                  }
            
moduleDeclaration = do
                    { reserved sprightLexer "module"
                    ; name <- identifier sprightLexer
                    ; imports <- importList
                    ; members <- moduleBody
                    ; eof
                    ; return $ Ast.Module name imports members
                    }

moduleBody = many moduleMember

moduleMember = fmap Ast.ModuleClass (try classDeclaration) <|> fmap Ast.ModuleFunction functionDeclaration
