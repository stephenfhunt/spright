module Spright.Parser (parseSprightText, parseSprightFile) where

import Text.ParserCombinators.Parsec (parse)
import Text.Parsec.Error (ParseError)

import Spright.Parser.Module
import qualified Spright.Ast as Ast

parseSprightText :: String -> Either ParseError Ast.Module
parseSprightText text = parse sprightModule "" text

parseSprightFile :: String -> IO (Either ParseError Ast.Module)
parseSprightFile filename = do 
                           { text <- readFile filename
                           ; return $ parseSprightText text
                           }

